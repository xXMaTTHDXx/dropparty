package me.xxmatthdxx.dropparty.cmds;

import me.xxmatthdxx.dropparty.DropParty;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * DropParty
 * Created By MatthewCameron
 * On 2015-01-05
 */
public class CommandManager implements CommandExecutor {


    private static DropParty plugin = DropParty.getPlugin();
    int ticks;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("dp") || cmd.getName().equalsIgnoreCase("dropparty")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "Player only command!");
                return true;
            }

            Player p = (Player) sender;

            if (args.length == 0) {

                if (!p.hasPermission("dp.admin")) {
                    p.sendMessage(ChatColor.GRAY + "=============" + ChatColor.GOLD + " Drop Party Help " +
                            ChatColor.GRAY + "==============");

                    p.sendMessage(ChatColor.GOLD + "/dp info" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "i" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows information about upcoming drop party." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Displays this help screen." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help admin" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h, a" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows admin only commands." + ChatColor.GRAY + ")");
                    return true;
                } else {

                    p.sendMessage(ChatColor.GRAY + "=============" + ChatColor.GOLD + " Drop Party Admin Help " +
                            ChatColor.GRAY + "==============");

                    p.sendMessage(ChatColor.GOLD + "/dp info" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "i" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows information about upcoming drop party." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Displays this help screen." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help admin" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h, a" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows admin only commands." + ChatColor.GRAY + ")");


                    p.sendMessage(ChatColor.GOLD + "/dp start" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "s" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Starts the drop party prematurely." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp set" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "set" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Multiple options to set." + ChatColor.GRAY + ")");

                    return true;
                }
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("help")) {

                    p.sendMessage(ChatColor.GRAY + "=============" + ChatColor.GOLD + " Drop Party Help " +
                            ChatColor.GRAY + "==============");

                    p.sendMessage(ChatColor.GOLD + "/dp info" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "i" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows information about upcoming drop party." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Displays this help screen." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help admin" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h, a" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows admin only commands." + ChatColor.GRAY + ")");
                    return true;
                } else if (args[0].equalsIgnoreCase("info")) {
                    //TODO Load info from config.

                    int votesRemaining = (plugin.getConfig().getInt("votesForDP") - plugin.getConfig().getInt("currentVotes"));
                    int mpVotesRemaining = (plugin.getConfig().getInt("votesForMP") - plugin.getConfig().getInt("currentMPVotes"));

                    p.sendMessage(ChatColor.GOLD + "==============" + ChatColor.DARK_AQUA + "Drop Party Info ==============");
                    p.sendMessage(ChatColor.GRAY + "Mine Party Votes: " + ChatColor.GOLD + plugin.getConfig().getInt("currentMPVotes") + ChatColor.GRAY + "/" +
                            ChatColor.GOLD + plugin.getConfig().getInt("votesForMP"));
                    p.sendMessage(ChatColor.GRAY + "Drop Party Votes: " + ChatColor.GOLD + plugin.getConfig().getInt("currentVotes") + ChatColor.GRAY + "/" +
                    ChatColor.GOLD + plugin.getConfig().getInt("votesForDP"));
                    p.sendMessage(ChatColor.GRAY + "Mine Party Votes remaining: " + ChatColor.GOLD + mpVotesRemaining);
                    p.sendMessage(ChatColor.GRAY + "Drop Party Votes remaining: " + ChatColor.GOLD + votesRemaining);
                    return true;
                } else if (args[0].equalsIgnoreCase("start")) {
                    ticks = 10;
                    new BukkitRunnable() {
                        public void run() {
                            if (ticks > 0) {
                                ticks--;
                            }

                            Bukkit.broadcastMessage(ChatColor.AQUA + "DP Starting in " + ChatColor.GOLD + ticks + ChatColor.AQUA + " seconds.");

                            if (ticks == 0) {
                                cancel();

                                for (String command : plugin.getConfig().getStringList("commandsOnVote")) {
                                    for (Player pl : Bukkit.getOnlinePlayers()) {
                                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("{PLAYER_NAME}", pl.getName()));
                                    }
                                }

                                plugin.getConfig().set("currentVotes", 0);
                                plugin.saveConfig();
                            }
                        }
                    }.runTaskTimer(plugin, 0L, 20L);
                }
            }

            // set votesNeeded 10
            else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("help") && args[1].equalsIgnoreCase("admin")) {

                    if (!p.hasPermission("dropparty.admin")) {
                        p.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "No Permission!");
                        return true;
                    }

                    p.sendMessage(ChatColor.GRAY + "=============" + ChatColor.GOLD + " Drop Party Admin Help " +
                            ChatColor.GRAY + "==============");

                    p.sendMessage(ChatColor.GOLD + "/dp info" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "i" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows information about upcoming drop party." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Displays this help screen." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp help admin" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "h, a" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Shows admin only commands." + ChatColor.GRAY + ")");


                    p.sendMessage(ChatColor.GOLD + "/dp start" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "s" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Starts the drop party prematurely." + ChatColor.GRAY + ")");

                    p.sendMessage(ChatColor.GOLD + "/dp set" + ChatColor.GRAY + " - " + ChatColor.GOLD + " Aliases" +
                            ChatColor.GRAY + " (" + ChatColor.GOLD + "set" + ChatColor.GRAY + ") - " +
                            ChatColor.GOLD + " Description " + ChatColor.GRAY + "(" + ChatColor.GOLD +
                            "Multiple options to set." + ChatColor.GRAY + ")");

                    return true;


                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("set")) {

                    if (!p.hasPermission("dropparty.admin")) {
                        p.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "No Permission!");
                        return true;
                    }

                    if (args[1].equalsIgnoreCase("mpVotes")) {


                        String path = "votesForMP";

                        try {
                            plugin.getConfig().set(path, Integer.valueOf(args[2]));
                            plugin.saveConfig();

                            p.sendMessage(ChatColor.GREEN + "Set the required mine party votes to " + ChatColor.GRAY + "(" + ChatColor.GOLD + args[2] +
                                    ChatColor.GRAY + ")");
                            return true;
                        } catch (NumberFormatException e) {
                            p.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED +
                                    "Please use a valid number for the second argument.");
                        }
                    }
                    else if(args[1].equalsIgnoreCase("dpVotes")){
                        String path = "votesForDP";

                        try {
                            plugin.getConfig().set(path, Integer.valueOf(args[2]));
                            plugin.saveConfig();

                            p.sendMessage(ChatColor.GREEN + "Set the required drop party votes to " + ChatColor.GRAY + "(" + ChatColor.GOLD + args[2] +
                                    ChatColor.GRAY + ")");
                            return true;
                        } catch (NumberFormatException e) {
                            p.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED +
                                    "Please use a valid number for the second argument.");
                        }
                    }
                    else {
                        p.sendMessage(ChatColor.RED + "/dp set [dpvotes,mpvotes] {amount}");
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
