package me.xxmatthdxx.dropparty;

import me.xxmatthdxx.dropparty.cmds.CommandManager;
import me.xxmatthdxx.dropparty.listeners.JoinListener;
import me.xxmatthdxx.dropparty.listeners.VoteListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

/**
 * DropParty
 * Created By MatthewCameron
 * On 2015-01-05
 */
public class DropParty extends JavaPlugin {
    /**
     * votesForDP: 10
     * currentVotes: 2
     * commandsOnVote:
     * - 'give {PLAYER_NAME} diamond'
     * - 'pay {PLAYER_NAME} 100'
     * - 'mrl reset mineparty'
     * includeAdminsInDP: true
     */

    Logger logger = Bukkit.getLogger();

    private static DropParty plugin;

    public void onEnable() {
        plugin = this;
        logger.info("Created by MaTTHD - http://ocga.me");

        if (Bukkit.getPluginManager().getPlugin("Votifier") == null) {
            Bukkit.getPluginManager().disablePlugin(this);
            System.out.println("Plugin depends on Votifier! Please install it to continue.");
        }

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new JoinListener(), this);
        pm.registerEvents(new VoteListener(), this);

        CommandManager cm = new CommandManager();

        getCommand("dp").setExecutor(cm);
        getCommand("dropparty").setExecutor(cm);

        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
    }

    public void onDisable() {
        plugin = null;

    }

    public static DropParty getPlugin() {
        return plugin;
    }
}
