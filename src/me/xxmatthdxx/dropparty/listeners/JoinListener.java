package me.xxmatthdxx.dropparty.listeners;

import me.xxmatthdxx.dropparty.ScoreboardUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * DropParty
 * Created By MatthewCameron
 * On 2015-01-05
 */
public class JoinListener implements Listener {

    ScoreboardUtil sbUtil = ScoreboardUtil.getInstance();

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        sbUtil.reloadBoard();
    }
}
