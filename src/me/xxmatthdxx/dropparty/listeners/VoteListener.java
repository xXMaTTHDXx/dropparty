package me.xxmatthdxx.dropparty.listeners;

import com.apple.concurrent.Dispatch;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import me.xxmatthdxx.dropparty.DropParty;
import me.xxmatthdxx.dropparty.ScoreboardUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * DropParty
 * Created By MatthewCameron
 * On 2015-01-05
 */
public class VoteListener implements Listener {

    //TODO Write this class.

    private static DropParty plugin = DropParty.getPlugin();

    int ticks;
    int currentIndex = plugin.getConfig().getInt("currentIndex");
    int totalVotes = plugin.getConfig().getInt("votesForDP");


    @EventHandler(priority = EventPriority.HIGH)
    public void onVote(VotifierEvent e) {


        int currentMPVotes = plugin.getConfig().getInt("currentMPVotes");
        int totalMPVotes = plugin.getConfig().getInt("votesForMP");
        final int remainingMPVotes = (totalMPVotes - currentMPVotes);

        int currentVotes = plugin.getConfig().getInt("currentVotes");
        int totalVotes = plugin.getConfig().getInt("votesForDP");
        final int remainingVotes = (totalVotes - currentVotes);

        plugin.getConfig().set("currentVotes", currentVotes + 1);
        plugin.getConfig().set("currentMPVotes", currentMPVotes + 1);
        plugin.saveConfig();
        ScoreboardUtil.getInstance().reloadBoard();

        ticks = 10;

        if (remainingVotes - 1 == 0) {

            plugin.getConfig().set("currentVotes", 0);
            plugin.saveConfig();
            ScoreboardUtil.getInstance().reloadBoard();

            new BukkitRunnable() {
                public void run() {
                    if (ticks > 0) {
                        ticks--;
                    }
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("broadcastDPMessage").replace("{TICKS}", "" + ticks)));

                    if (ticks == 0) {
                        cancel();
                        ticks = 10;
                        for (String command : plugin.getConfig().getStringList("commandsOnVote")) {
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("{PLAYER_NAME}", player.getName()));
                            }
                        }
                    }
                }
            }.runTaskTimer(plugin, 0L, 20L);


        } else if (remainingMPVotes - 1 == 0) {

            plugin.getConfig().set("currentMPVotes", 0);
            plugin.saveConfig();
            ScoreboardUtil.getInstance().reloadBoard();

            new BukkitRunnable() {
                public void run() {
                    if (ticks > 0) {
                        ticks--;
                    }
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("broadcastMPMessage").replace("{TICKS}", "" + ticks)));

                    if (ticks == 0) {
                        cancel();

                        for (String command : plugin.getConfig().getStringList("commandsOnMPVote")) {
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("{PLAYERNAME}", player.getName()));
                            }
                        }
                    }
                }
            }.runTaskTimer(plugin, 0L, 20L);


        }
    }

    public int getIndex() {
        for (int i = 0; i < plugin.getConfig().getStringList("dpList").size(); i++) {
            if (totalVotes == Integer.parseInt(plugin.getConfig().getStringList("dpList").get(i))) {
                return i;
            }
        }
        return -1;
    }
}
