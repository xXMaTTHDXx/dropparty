package me.xxmatthdxx.dropparty;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * DropParty
 * Created By MatthewCameron
 * On 2015-01-05
 */
public class ScoreboardUtil {

    private static DropParty plugin = DropParty.getPlugin();

    private static ScoreboardUtil instance = new ScoreboardUtil();

    public static ScoreboardUtil getInstance() {
        return instance;
    }

    public void reloadBoard() {

        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();
        Objective obj = board.registerNewObjective("dummy", "Votes");

        obj.setDisplayName(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("scoreboardTitle")));
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        int totalVotesInt = plugin.getConfig().getInt("votesForDP");
        int currentVotesInt = plugin.getConfig().getInt("currentVotes");

        int totalMPVotesInt = plugin.getConfig().getInt("votesForMP");
        int currentMPVotesInt = plugin.getConfig().getInt("currentMPVotes");

        Score totalVotes = obj.getScore(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("dpVoteScoreboardHeader")));
        totalVotes.setScore(-1);

        Score totalVoteNumber = obj.getScore(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("dpVoteNumberColor")) + "" + currentVotesInt + "/" + totalVotesInt);
        totalVoteNumber.setScore(-2);

        Score mpVotes = obj.getScore(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("mpVoteScoreboardHeader")));
        mpVotes.setScore(-3);

        Score mpVoteNumber = obj.getScore(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("mpVoteNumberColor")) + "" + currentMPVotesInt + "/" + totalMPVotesInt);
        mpVoteNumber.setScore(-4);

        for (Player p : Bukkit.getOnlinePlayers()) {
            p.setScoreboard(board);
        }
    }
}
